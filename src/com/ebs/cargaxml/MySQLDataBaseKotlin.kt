package com.ebs.cargaxml

import java.io.File
import java.io.IOException
import java.io.InputStream
import java.sql.*
import java.util.Properties
import java.io.FileInputStream


/**
 * Program to list databases in MySQL using Kotlin
 */
object MySQLDatabaseExampleKotlin {

    internal var conn: Connection? = null
    internal var username = "" // provide the username
    internal var password = "" // provide the corresponding password
    internal var host = "localhost"
    internal var port = "3306"
    internal var database = "DB_NAME_HERE"
    internal var path = "/home/eflores/cargaxml/"
    internal var pathData = "/home/eflores/cargaxml/"
    internal var pathDataXmlsSAT = "/home/eflores/cargaxml/dataxmlSAT/"

    internal val ACTUALIZAR_XML_SAT = 0
    internal var operacion = -1


    @JvmStatic
    fun main(args: Array<String>) {
        operacion = -1
        if(args.size == 1){
            var flagRecieved = args[0];
            if(flagRecieved == "-xmlsat"){
                println("USING: $pathDataXmlsSAT")
                println("UPDATING XML SAT")
                pathData = pathDataXmlsSAT
                operacion =  ACTUALIZAR_XML_SAT
            }
            // make a connection to MySQL Server
            getConnection()
            // execute the query via connection object
            /// executeMySQLQuery()
            readLineByLine()
            closeConnection()
        } else {
            println("POR FAVOR USE -xml(para indicar la carg de xml SAT) o agregue nuevas operaciones al programa ")
        }

    }

    fun readLineByLine() {
        val inputStream: InputStream = File("${path}uuids.txt").inputStream()
        val lineList = mutableListOf<String>()

        inputStream.bufferedReader().useLines { lines ->
            lines.forEach {
                lineList.add(it)///AGREGA CADA LINEA AL LINELIST
            }
        }
        lineList.forEach {
            ///iTERAMOS CADALINEA
            println(">  " + it)
            var split = it.split(",")
            if (checkIfExistUuid(split[0]) && checkIfExistFile(split[0])) {
                if(operacion == ACTUALIZAR_XML_SAT){
                    updateXmlSatIntoFiles(split[0])
                } else{
                    println("Operacion desconocida")
                }
            }
        } ///iTERAMOS CADALINEA

    }

    fun checkIfExistUuid(uuid: String): Boolean {
        var stmt: PreparedStatement? = null
        var resultset: ResultSet? = null

        try {
            stmt = conn!!.prepareStatement("SELECT UUID FROM  M_CFD_XML WHERE UUID = ? LIMIT 1")
            stmt.setString(1, uuid)
            resultset = stmt!!.executeQuery()

            while (resultset!!.next()) {///Ok uuid exist
                println(resultset.getString("UUID"))
                return true
            }
        } catch (ex: SQLException) {
            // handle any errors
            ex.printStackTrace()
        } finally {
            // release resources
            if (resultset != null) {
                try {
                    resultset.close()
                } catch (sqlEx: SQLException) {
                }

                resultset = null
            }

            if (stmt != null) {
                try {
                    stmt.close()
                } catch (sqlEx: SQLException) {
                }

                stmt = null
            }
        }
        println("MCFDXML not found for uuid:$uuid")
        return false;
    }

    fun checkIfExistFile(uuid: String): Boolean {
        val f = File("${pathData}${uuid}.xml")
        if (f.isFile && f.canRead()) {
            try {
                /*
                // Open the stream.
                val `in` = FileInputStream(f)
                // To read chars from it, use new InputStreamReader
                // and specify the encoding.
                try {
                    // Do something with in.
                } finally {
                    `in`.close()
                }*/
            } catch (ex: IOException) {
                // Appropriate error handling here.
            }
            return true
        }
        println("File not found for uuid $uuid")
        return false
    }

    /**
     * Actualizacion del xml SAT
     */
    fun updateXmlSatIntoFiles(uuid: String) {
        var stmt: PreparedStatement? = null
        var resultset: ResultSet? = null

        try {
            stmt = conn!!.prepareStatement("UPDATE M_CFD_XML SET XML = ? WHERE UUID = ? LIMIT 1")
            stmt.setString(2, uuid)

            val file = File("${pathData}${uuid}.xml")
            val inputStream = FileInputStream(file)
            stmt.setBlob(1, inputStream)
            stmt.setString(2, uuid)
            if (1 == stmt!!.executeUpdate()) {
                println("UUID XML SAT:$uuid updated")
            } else {
                println("Error on update")
            }
        } catch (ex: SQLException) {
            // handle any errors
            ex.printStackTrace()
        } catch (ex: IOException) {
            // handle any errors
            ex.printStackTrace()
        } finally {
            // release resources
            if (resultset != null) {
                try {
                    resultset.close()
                } catch (sqlEx: SQLException) {
                }

                resultset = null
            }

            if (stmt != null) {
                try {
                    stmt.close()
                } catch (sqlEx: SQLException) {
                }

                stmt = null
            }
        }
    }

    fun executeMySQLQuery() {
        var stmt: Statement? = null
        var resultset: ResultSet? = null

        try {
            stmt = conn!!.createStatement()
            resultset = stmt!!.executeQuery("SHOW DATABASES;")

            if (stmt.execute("SHOW DATABASES;")) {
                resultset = stmt.resultSet
            }

            while (resultset!!.next()) {
                println(resultset.getString("Database"))
            }
        } catch (ex: SQLException) {
            // handle any errors
            ex.printStackTrace()
        } finally {
            // release resources
            if (resultset != null) {
                try {
                    resultset.close()
                } catch (sqlEx: SQLException) {
                }

                resultset = null
            }

            if (stmt != null) {
                try {
                    stmt.close()
                } catch (sqlEx: SQLException) {
                }

                stmt = null
            }
        }
    }

    fun closeConnection() {
        println(">   Closing connection")
        if (conn != null) {
            try {
                conn!!.close()
            } catch (sqlEx: SQLException) {
            }

            conn = null
        }
    }

    /**
     * This method makes a connection to MySQL Server
     * In this example, MySQL Server is running in the local host (so 127.0.0.1)
     * at the standard port 3306
     */
    fun getConnection() {
        println("> openning connection")
        val connectionProps = Properties()
        connectionProps.put("user", username)
        connectionProps.put("password", password)
        try {
            Class.forName("com.mysql.jdbc.Driver").newInstance()
            conn = DriverManager.getConnection(
                    "jdbc:" + "mysql" + "://" +
                            host +
                            ":" + port + "/" +
                            database,
                    connectionProps)
        } catch (ex: SQLException) {
            // handle any errors
            ex.printStackTrace()
        } catch (ex: Exception) {
            // handle any errors
            ex.printStackTrace()
        }
    }
}